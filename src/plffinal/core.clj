(ns plffinal.core)


;;Funciones generales

(defn tabla-posiciones
  []
  (hash-map \< [-1 0]
            \v [0 -1]
            \> [1 0]
            \^ [0 1]))

(defn coordenadas
  [x]
  (letfn [(p [s] (replace (tabla-posiciones) s))

          (f [sec acc]
            (if (empty? sec)
              [[0 0]]
              (g (conj acc [0 0] (first (p sec))) (rest (p sec)))))

          (g [aux tp]
            (if (empty? tp)
              aux
              (g (conj aux [(+ (first (last aux)) (first (first tp))) (+ (second (last aux)) (second (first tp)))]) (rest tp))))]
    (f x [])))


;;Solución al problema 1

(defn regresa-al-punto-de-origen?-viejo
  [x]
  (let [f (fn [] (frequencies x))
        g (fn [] (and (== (get (f) \v 0) (get (f) \^ 0))
                      (== (get (f) \< 0) (get (f) \> 0))))]
    (g)))

(defn regresa-al-punto-de-origen?
  [x]
  (let [f (fn [] (= (last (coordenadas x)) [0 0]))]
    (f)))

; Solución al problema 2
(defn regresan-al-punto-de-origen?
  [& args]
  (every? true? (map regresa-al-punto-de-origen? args)))

;Solución al problema 3
(defn tabla-flechas
  []
  (hash-map \> \<
            \< \>
            \v \^
            \^ \v))

(defn regreso-al-punto-de-origen
  [x]
  (let [f (fn [] (if (true? (regresa-al-punto-de-origen? x))
                   (list)
                   (replace (tabla-flechas) (reverse x))))]
    (f)))

;Solución problema 4
(defn mismo-punto-final?-viejo
  [sa sb]
  (let [f (fn [s] (frequencies s))
        x (fn [s] (- (get (f s) \> 0) (get (f s) \< 0)))
        y (fn [s] (- (get (f s) \^ 0) (get (f s) \v 0)))
        p (fn [] (and (== (x sa) (x sb)) (== (y sa) (y sb))))]
    (p)))

(defn mismo-punto-final?
  [sa sb]
  (let [f (fn [] (= (last (coordenadas sa)) (last (coordenadas sb))))]
    (f)))

;Solución problema 5

(defn coincidencias
  [sa sb]
  (let [f (fn [] (concat (coordenadas sa) (coordenadas sb)))
        g (fn [] (vals (frequencies (f))))
        h (fn [] (count (filterv (fn [x]
                                    (> x 1)) (g))))]
    (h)))
