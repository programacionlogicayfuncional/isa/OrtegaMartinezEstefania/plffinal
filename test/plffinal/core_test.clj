(ns plffinal.core-test
  (:require [clojure.test :refer [deftest is testing]]
            [plffinal.core :refer :all]))





 (deftest coordenadas-test
  (testing "Pruebas con secuencias vacías."
    (is (= [[0 0]] (coordenadas "")))
    (is (= [[0 0]] (coordenadas [])))
    (is (= [[0 0]] (coordenadas (list)))))

  (testing "Pruebas con secuencias de un elemento"
    (is (= [[0 0] [-1 0]] (coordenadas "<")))
    (is (= [[0 0] [1 0]] (coordenadas ">")))
    (is (= [[0 0] [0 1]] (coordenadas "^")))
    (is (= [[0 0] [0 -1]] (coordenadas "v"))))

  (testing "Pruebas con más de un elemento en la secuencia"
    (is (= [[0 0] [-1 0] [-2 0] [-3 0]] (coordenadas "<<<")))
    (is (= [[0 0] [1 0] [1 -1] [1 -2] [0 -2]] (coordenadas ">vv<")))
    (is (= [[0 0] [0 1] [0 2] [0 1] [0 0] [0 -1] [-1 -1]] (coordenadas "^^vvv<")))
    (is (= [[0 0] [0 -1] [0 -2] [-1 -2] [0 -2] [1 -2] [1 -1]] (coordenadas "vv<>>^")))))
 

 (deftest regresa-al-punto-de-origen?-test
   (testing "Pruebas sin desplazamiento"
     (is (= true (regresa-al-punto-de-origen? "")))
     (is (= true (regresa-al-punto-de-origen? [])))
     (is (= true (regresa-al-punto-de-origen? (list)))))

   (testing "Pruebas con desplazamiento"
     (is (= true (regresa-al-punto-de-origen? "><")))
     (is (= true (regresa-al-punto-de-origen? (list \> \<))))
     (is (= true (regresa-al-punto-de-origen? "v^")))
     (is (= true (regresa-al-punto-de-origen? [\v \^])))
     (is (= true (regresa-al-punto-de-origen? "^>v<")))
     (is (= true (regresa-al-punto-de-origen? (list \^ \> \v \<))))
     (is (= true (regresa-al-punto-de-origen? "<<vv>>^^")))
     (is (= true (regresa-al-punto-de-origen? [\< \< \v \v \> \> \^ \^])))

     (is (= false (regresa-al-punto-de-origen? ">")))
     (is (= false (regresa-al-punto-de-origen? (list \>))))
     (is (= false (regresa-al-punto-de-origen? "<^")))
     (is (= false (regresa-al-punto-de-origen? [\< \^])))
     (is (= false (regresa-al-punto-de-origen? ">>><<")))
     (is (= false (regresa-al-punto-de-origen? (list \> \> \> \< \<))))
     (is (= false (regresa-al-punto-de-origen? [\v \v \^ \^ \^])))))

(deftest regresan-al-punto-de-origen?-test
  (testing "Pruebas con n secuencias sin desplazamiento"
    (is (= true (regresan-al-punto-de-origen?)))
    (is (= true (regresan-al-punto-de-origen? [])))
    (is (= true (regresan-al-punto-de-origen? "")))
    (is (= true (regresan-al-punto-de-origen? [] "" (list))))
    (is (= true (regresan-al-punto-de-origen? "" "" "" "" [] [] [] (list) ""))))

  (testing "Pruebas con n secuencias desplazamiento"
    (is (= true (regresan-al-punto-de-origen? ">><<" [\< \< \> \>] (list \^ \^ \v \v))))
    (is (= true (regresan-al-punto-de-origen? ">><<" "v<^>" "^^^>>>vvv<<<" "^>>v<<^v" "<>v^")))
    (is (= true (regresan-al-punto-de-origen? [\> \> \< \<] [\v \< \^ \>] [\< \> \v \^])))
    (is (= true (regresan-al-punto-de-origen? (list \> \> \< \<) (list \v \< \^ \>) (list \< \> \v \^))))

    (is (= false (regresan-al-punto-de-origen? (list \< \>) "^^" [\> \<])))
    (is (= false (regresan-al-punto-de-origen? ">>>" "^vv^" "<<>>")))
    (is (= false (regresan-al-punto-de-origen? [\< \< \> \> \> \> \> \> \> \>])))
    (is (= false (regresan-al-punto-de-origen? "<" (list \>) [\v] [\^])))
    (is (= false (regresan-al-punto-de-origen? "<^" (list \> \> \> \< \<) "><")))))

(deftest regreso-al-punto-de-origen-test
  (testing "Pruebas con secuencias que vuelven al punto de origen"
    (is (= () (regreso-al-punto-de-origen "")))
    (is (= () (regreso-al-punto-de-origen [])))
    (is (= () (regreso-al-punto-de-origen (list))))
    (is (= () (regreso-al-punto-de-origen (list \^ \^ \^ \> \< \v \v \v))))
    (is (= () (regreso-al-punto-de-origen [\^ \> \v \<]))))

  (testing "Pruebas con secuencias que no vuelven al punto de origen"
    (is (= '(\< \< \<) (regreso-al-punto-de-origen ">>>")))
    (is (= '(\< \< \^ \^ \^ \>) (regreso-al-punto-de-origen [\< \v \v \v \> \>])))
    (is (= '(\^ \^ \< \^ \< \^) (regreso-al-punto-de-origen [\v \> \v \> \v \v])))
    (is (= '(\< \< \v \v \v) (regreso-al-punto-de-origen (list \^ \^ \^ \> \>))))))

(deftest mismo-punto-final?-test
  (testing "Pruebas con secuencias que terminan en el mismo punto final"
    (is (= true (mismo-punto-final? "" [])))
    (is (= true (mismo-punto-final? "^^^" "<^^^>")))
    (is (= true (mismo-punto-final? [\< \< \< \>] (list \< \<))))
    (is (= true (mismo-punto-final? (list \< \v \>) (list \> \v \<)))))

  (testing "Pruebas con secuencias que no tienen el mismo punto final"
    (is (= false (mismo-punto-final? "" "<")))
    (is (= false (mismo-punto-final? [\> \>] "<>")))
    (is (= false (mismo-punto-final? [\> \> \>] [\> \> \> \>])))
    (is (= false (mismo-punto-final? (list) (list \^))))))

(deftest coincidencias-test
  (testing "Pruebas de dos secuencias que coinciden en un solo punto"
    (is (= 1 (coincidencias "" [])))
    (is (= 1 (coincidencias (list \< \<) [\> \>]))))

  (testing "Pruebas de dos secuencias que coinciden en más de un punto"
    (is (= 2 (coincidencias [\^ \> \> \> \^] ">^^<")))
    (is (= 4 (coincidencias "<<vv>>^>>" "vv<^")))
    (is (= 6 (coincidencias ">>>>>" [\> \> \> \> \>])))
    (is (= 6 (coincidencias [\> \> \> \> \>] (list \> \> \> \> \> \> \^ \^ \^ \^))))
    (is (= 8 (coincidencias "<<vv>>^" "vv<<^^>")))))
